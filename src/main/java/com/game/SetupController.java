package com.game;

import com.game.core.SetupModel;
import com.game.core.WorldController;
import com.game.core.WorldModel;
import com.game.core.WorldView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class SetupController {

    public boolean isSetupComplete() {
        return setupComplete;
    }

    private SetupView setupView;
    private SetupModel setupModel;
    private boolean setupComplete;

    public SetupController(SetupView setupView, SetupModel setupModel) {
        this.setupView = setupView;
        this.setupModel = setupModel;
        setupComplete = false;

        this.setupView.addSubmitListener(new SubmitListener());
        this.setupView.setStoryText(setupModel.returnIntroText() + setupModel.getPlayableCharactersText());
    }

    class SubmitListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if(setupModel.getSetupStage() == 0 ){
                setupModel.setPlayerCount(1);
                setupModel.setSetupStage(1);
            }
            if (setupModel.getSetupStage() == 1) {
                if (!setupModel.setupPlayableCharacters(setupView.getInputText())) {
                    setupView.setStoryText("<html>Enter a valid character type maggot!<br>" + setupModel.getPlayableCharactersText() + "</html>");
                } else {
                    setupView.clearInput();
                    setupView.setStoryText(String.format("Input your characters name for the %s", setupModel.getPlayers().get(0).getName()));
                    setupModel.setSetupStage(2);
                    return;
                }
            }

            if (setupModel.getSetupStage() == 2) {
                if (!setupModel.setupPlayerNames(setupView.getInputText())) {
                    setupView.setStoryText("You need to enter a better name than that!");
                } else {
                    setupView.setContinueText();
                    setupModel.setSetupStage(3);
                    setupView.setStoryText(setupModel.listStats(setupModel.getPlayers().get(0)) + setupModel.listPlayableCharacters());
                    return;
                }
            }
            if(setupModel.getSetupStage() == 3){
                WorldModel worldModel = setupModel.createWorldModel();
                WorldView worldView = setupView.createStoryView();
                WorldController worldController = new WorldController(worldView, worldModel);
            }
        }
    }
}



