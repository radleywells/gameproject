package com.game;

import com.game.core.WorldView;
import com.game.environment.World;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class SetupView extends JFrame {

    private JTextField input = new JTextField(10);
    private JLabel storyText = new JLabel("", SwingConstants.CENTER);
    private JButton submit = new JButton("Submit");

    public SetupView(){

        JPanel introPanel = new JPanel();
        introPanel.setLayout(new BorderLayout());
        this.setSize(1500, 800);


        Font storyFont = new Font(Font.DIALOG_INPUT, Font.PLAIN, 25);
        Font inputFont = new Font(Font.DIALOG_INPUT, Font.PLAIN, 20);
        storyText.setFont(storyFont);
        input.setFont(inputFont);
        introPanel.add(submit, BorderLayout.SOUTH);
        introPanel.add(input, BorderLayout.SOUTH);
        input.setPreferredSize(new Dimension(50,50));
        introPanel.add(storyText, BorderLayout.CENTER);
        this.getRootPane().setDefaultButton(submit);

        this.add(introPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setVisible(true);
    }

     void setStoryText(String text){
        storyText.setText(text);

     }

     public WorldView createStoryView(){
         return new WorldView();
     }

     void setContinueText(){
        input.setText("Press ENTER to continue..");
     }

    String getInputText(){
        return input.getText();
    }

    void clearInput(){
        input.setText("");
    }

    void addSubmitListener(ActionListener listenerForSubmit){
        submit.addActionListener(listenerForSubmit);
    }

    void displayErrorMessage(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage);
    }

}
