package com.game.characters;

import com.game.environment.Utilities;
import com.game.items.Inventory;

public class Blacksmith extends PlayableCharacter {

    public Blacksmith(){
        characterType = "Blacksmith";
        name = "Blacksmith";
        hitPoints = 12;
        isDead = false;
        level = 1;
        classAbility = "Anvil Smash";

        //stats
        damage = 2;
        strength = 16;
        dexterity = 9;
        intelligence = 11;
        inventory = new Inventory();
    }

    public void greet() {
        System.out.println(String.format("Hello I am  a %s! My name is %s and my passion is making stuff with metal, hell yeah that's what I love!", characterType, getName()));
    }

    public int classAbility(Character character){
        Utilities util = new Utilities();
        util.storyText(String.format("You use your %s skill %s!", getCharacterType(), getClassAbility()));
        return getStrength() / 4;
    }

}