package com.game.characters;

import com.game.items.Armor;
import com.game.items.Inventory;
import com.game.items.Weapon;

public abstract class Character implements Hostile{

    Inventory inventory;
    String characterType;
    boolean isDead;
    int hitPoints;
    int level;
    int damage;
    String name;
    boolean isTurn = false;
    int battleCount = 0;
    String classAbility;


    public boolean isTurn() {
        return isTurn;
    }

    public void setTurn(boolean turn) {
        isTurn = turn;
    }

    public void attack(Character character) {
        int weaponDamage = this.getInventory().getEquippedWeapon().getDamage();
        character.setHitPoints(character.getHitPoints() - weaponDamage);
    }

    public void talk(String text){
        System.out.println(String.format("%s%s: %s","\n", this.name, text));
    }

    public Weapon getEquippedWeapon(){
        return inventory.getEquippedWeapon();
    }

    public Armor getEquippedArmor(){
        return inventory.getEquippedArmor();
    }

    public Inventory getInventory() {
        return inventory;
    }
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }
    public int getDamage() {
        return damage;
    }
    public void setDamage(int damage) {
        this.damage = damage;
    }
    public int getLevel() {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public boolean isDead() {
        return isDead;
    }
    public void setIsDead(boolean isDead) {
        this.isDead = isDead;
    }
    public void setCharacterType(String characterType) {
        this.characterType = characterType;
    }
    public String getCharacterType(){
        return this.characterType;
    };
    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }
    public int getHitPoints() {
        return this.hitPoints;
    }
    public void setName(String name) {
        this.name = name
        ;}
    public String getName() { return this.name;}
    public int getBattleCount() {
        return battleCount;
    }
    public void setBattleCount(int battleCount) {
        this.battleCount = battleCount;
    }

    public String getClassAbility(){
        return classAbility;
    }
    public abstract int classAbility(Character character);
}
