package com.game.characters;

import com.game.items.Armor;
import com.game.items.Weapon;
import java.util.Arrays;
import java.util.List;

public class CharacterManager {

    private List<String> characterTypes = Arrays.asList("Blacksmith", "Chef", "Lumberjack", "Tavern Owner") ;
    private final List<PlayableCharacter> playableCharacters = Arrays.asList(new Blacksmith(), new Lumberjack(), new Chef(), new TavernOwner());

    public List<String> getCharacterTypes(){
        return this.characterTypes;
    }

    public PlayableCharacter createCharacterFromType(String type){
        PlayableCharacter newCharacter = null;

        for (PlayableCharacter playableCharacter : playableCharacters){
            if(playableCharacter.getCharacterType().equals(type)){
                newCharacter = playableCharacter;
                equipStartGear(newCharacter);
            }
        }
        return newCharacter;
    }

    public void equipStartGear(PlayableCharacter p){

        Weapon weapon = new Weapon(2, 1, "Wooden Sword", true);
        p.getInventory().setEquippedWeapon(weapon);

        Armor armor = new Armor(2, "Wooden Armor", true);
        p.getInventory().setEquippedArmor(armor);
        }
    }
