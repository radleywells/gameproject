package com.game.characters;
import com.game.environment.Utilities;
import com.game.items.Inventory;

public class Chef extends PlayableCharacter {

    public Chef(){
        characterType = "Chef";
        name = "Chef";
        hitPoints = 9;
        isDead = false;
        level = 1;
        classAbility = "Butcher Knife Swipe";

        //stats
        damage = 1;
        strength = 8;
        dexterity = 16;
        intelligence = 13;
        inventory = new Inventory();
    }

    public void greet() {
        System.out.println(String.format("Hello I am a %s! My name is %s and I love cooking pies, and eating them too!", characterType, getName()));
    }

    public int classAbility(Character character){
        Utilities util = new Utilities();
        util.storyText(String.format("You use your %s skill %s!", getCharacterType(), getClassAbility()));
        return this.getDexterity() / 4;
    }

}
