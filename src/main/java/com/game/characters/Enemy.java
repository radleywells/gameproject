package com.game.characters;

import com.game.environment.Utilities;

public abstract class Enemy extends Character {
    Utilities util = new Utilities();

    public void taunt(Character player){
        talk(String.format("I'm gonna take that %s off of you %s scum!",
                player.getInventory().getEquippedWeapon().getName(),  player.getCharacterType()));
    }

    public int classAbility(Character character){

        util.storyText(String.format("You use your %s skill %s!", getCharacterType(), getClassAbility()));
        return level * 2;
    }
}
