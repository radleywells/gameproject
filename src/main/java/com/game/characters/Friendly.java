package com.game.characters;

import com.game.environment.Utilities;

public class Friendly extends Character {

    public Friendly(String name){
        this.name = name;
    }

    public void greet(PlayableCharacter p){
        Utilities util = new Utilities();
        this.talk(String.format("Hey %s!, my name is %s.", p.getName(), this.getName()));
    }

    public int classAbility(Character character){
        return 0;
    }
}
