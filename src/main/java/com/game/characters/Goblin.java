package com.game.characters;

import com.game.items.Inventory;

public class Goblin extends Enemy {

    public Goblin(String name) {
        this.name = name;
        hitPoints = 7;
        inventory = new Inventory();
        characterType = "Goblin";
    }
}
