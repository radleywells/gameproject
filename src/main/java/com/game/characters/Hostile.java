package com.game.characters;

public interface Hostile {

    void attack(Character character);

}
