package com.game.characters;

import com.game.items.Inventory;

public class Lumberjack extends PlayableCharacter {

    public Lumberjack(){
        characterType = "Lumberjack";
        name = "Lumberjack";
        hitPoints = 11;
        isDead = false;
        level = 1;
        classAbility = "Ferocious Chop";

        //stats
        damage = 2;
        strength = 14;
        dexterity = 8;
        intelligence = 10;
        inventory = new Inventory();
    }

    public void greet() {
        System.out.println(String.format("Hello I am  a %s! My name is %s and I love chopping wood oh boy i do!", characterType, getName()));
    }

    public int classAbility(Character character){

        return (getStrength() + getDexterity()) / 5;
    }

}
