package com.game.characters;

import java.util.ArrayList;
import java.util.List;

public abstract class PlayableCharacter extends Character{

    int strength;
    int dexterity;
    int intelligence;

    public abstract void greet();

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }


    public List<String> getEquppedStatsAndEquipmentLabels() {

        List<String> listOfValues = new ArrayList<>();
        listOfValues.add(this.getName());
        listOfValues.add(this.getCharacterType());
        listOfValues.add(this.getEquippedWeapon().getName());
        listOfValues.add(this.getEquippedArmor().getName());
        listOfValues.add("Level - " + Integer.toString(this.getLevel()));
        listOfValues.add("Strength - " + Integer.toString(this.getStrength()));
        listOfValues.add("Dexterity - " + Integer.toString(this.getDexterity()));
        listOfValues.add("Intelligence - " + Integer.toString(this.getIntelligence()));

        return listOfValues;
    }

}
