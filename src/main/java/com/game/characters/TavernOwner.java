package com.game.characters;

import com.game.environment.Utilities;
import com.game.items.Inventory;

public class TavernOwner extends PlayableCharacter {

    public TavernOwner(){
        characterType = "Tavern Owner";
        name = "Tavern Owner";
        hitPoints = 8;
        level = 1;
        isDead = false;
        classAbility = "Tankard Explosion";

        //stats
        damage = 1;
        strength = 10;
        dexterity = 15;
        intelligence = 16;
        inventory = new Inventory();
    }

    public void greet() {
        System.out.println(String.format("Hello I am  a %s! My name is %s - come spend a cosy night in my tavern and drink some beer, woo!", characterType, getName()));
    }

    public int classAbility(Character character){
        Utilities util = new Utilities();
        util.storyText(String.format("You use your %s skill %s!", getCharacterType(), getClassAbility()));
        return (getDexterity() + getIntelligence()) / 7;
    }
}
