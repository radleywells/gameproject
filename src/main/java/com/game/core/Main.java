package com.game.core;

import com.game.SetupController;
import com.game.SetupView;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        try {
            //here you can put the selected theme class name in JTattoo
            UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");

        } catch (Throwable t) {
            java.util.logging.Logger.getLogger(SetupView.class.getName()).log(java.util.logging.Level.SEVERE, null, t);
        }

        SetupView setupView = new SetupView();
        SetupModel model = new SetupModel();
        SetupController controller = new SetupController(setupView, model);
        setupView.setVisible(true);
    }
}



//        Run newGame = new Run();
//        newGame.start();
