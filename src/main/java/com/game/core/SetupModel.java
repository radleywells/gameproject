package com.game.core;

import com.game.characters.*;
import com.game.environment.Utilities;

import java.util.ArrayList;
import java.util.Scanner;

public class SetupModel {

    private int playerCount;
    private ArrayList<PlayableCharacter> players;
    private int setupStage = 0;

    public String returnIntroText() {
        StringBuilder sb = new StringBuilder();
        CharacterManager charList = new CharacterManager();
        charList.getCharacterTypes().forEach(x -> sb.append(x).append(" / "));
        return String.format("<html>............................SETUP STARTED............................<br><br>" +
                "Welcome to the land of limitless possibilities<br><br><br>Choose from the following list of character types:<br>%s</html>", sb.toString());
    }

    public void setPlayerCount(int count) {
        this.playerCount = count;
    }

    public int getPlayerCount() {
        return this.playerCount;
    }

    public ArrayList<PlayableCharacter> getPlayers() {
        return this.players;
    }

    public void addPlayer(PlayableCharacter p) {
        CharacterManager manager = new CharacterManager();
        players.add(p);
        manager.equipStartGear(p);
    }

    public String listPlayableCharacters() {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><br>Your party of destruction has the following adventurers: <br>");
        players.forEach(player -> sb.append(String.format("%s%s", player.getName(), "<br>")));
        sb.append("</html>");
        return sb.toString();
    }

    public String getPlayableCharactersText() {
        CharacterManager charList = new CharacterManager();
        StringBuilder sb = new StringBuilder();
        charList.getCharacterTypes().forEach(x -> sb.append(x).append(" / "));

        return String.format("<html>Choose from the following list of character types:<br>%s<br><br><br><br>Please input character: </html>", sb.toString());
    }

    public void setSetupStage(int stageNumber) {
        this.setupStage = stageNumber;
    }

    public int getSetupStage() {
        return setupStage;
    }

    public boolean setupPlayableCharacters(String input) {
        players = new ArrayList<>();
        CharacterManager charList = new CharacterManager();
            for (String characterType : charList.getCharacterTypes()) {
                if (input.toLowerCase().replaceAll("\\s+", "").equals(characterType.toLowerCase().replaceAll("\\s+", ""))) {
                    players.add(charList.createCharacterFromType(characterType));
                    return true;
                }
            }
            return false;
    }

    public boolean setupPlayerNames(String input) {
        input = input.trim();
        if (!input.equals("")) {
            this.players.get(0).setName(input.substring(0, 1).toUpperCase() + input.substring(1));
            return true;
        } else {
            return false;
        }
    }

        public String listStats (PlayableCharacter p) {
            return (String.format("<html>%s's stats are as follows:<br><br>Character Type: %s<br>Hit Points: %s<br>Dexterity: %s<br>Intelligence: %s<br>Strength: %s<br>",
                    p.getName(), p.getCharacterType(), p.getHitPoints(), p.getDexterity(), p.getIntelligence(), p.getStrength()));
        }

        public WorldModel createWorldModel(){
            return new WorldModel(this);
        }
    }

