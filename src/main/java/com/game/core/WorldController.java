package com.game.core;

import com.game.environment.Utilities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WorldController {

    WorldView worldView;
    WorldModel worldModel;
    Utilities util = new Utilities();

    public WorldController(WorldView worldView, WorldModel worldModel){
        this.worldView = worldView;
        this.worldModel = worldModel;

        this.worldView.addSubmitListener(new SubmitListener());
        worldView.setStats(worldModel.getStatsAndEquipmentValues());
        worldView.setStoryText("<html>As you are walking through the woods you hear a voice from amongst the trees...<html>");
        worldModel.setSetupStage(0);
    }

    class SubmitListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(worldModel.getSetupStage() == 0){
                worldView.setStoryText(
                    "<html>Welcome to the land of wonders my friends, you must be the ones that have been sent. " +
                    "The seer will explain why you have been called here.<br><br>Watch out there might be some " +
                    "enemies after that fine wooden equipment you've got there. In fact someone is approaching " +
                    "you now....<html>"
                );
                worldView.setStats(worldModel.getStatsAndEquipmentValues());
                worldView.setContinueText();
                worldModel.setSetupStage(1);
                return;
            }
            if(worldModel.getSetupStage() == 1){

            }

        }
    }

}
