package com.game.core;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class WorldView extends JFrame {

    private JTextField input = new JTextField(10);
    private JLabel storyText = new JLabel("", SwingConstants.CENTER);
    private JButton submit = new JButton("Submit");
    private JLabel stats = new JLabel();

    public WorldView(){

        JPanel introPanel = new JPanel();
        introPanel.setLayout(new BorderLayout());
        this.setSize(1500, 800);


        Font storyFont = new Font(Font.DIALOG_INPUT, Font.PLAIN, 28);
        Font defaultFont = new Font(Font.DIALOG_INPUT, Font.PLAIN, 20);
        storyText.setFont(storyFont);
        storyText.setBorder(new EmptyBorder(25, 25, 25, 25));
        input.setFont(defaultFont);
        stats.setFont(defaultFont);
        stats.setBorder(new EmptyBorder(10, 15, 10, 10));


        introPanel.add(submit, BorderLayout.SOUTH);
        introPanel.add(input, BorderLayout.SOUTH);
        introPanel.add(storyText, BorderLayout.CENTER);
        introPanel.add(stats, BorderLayout.WEST);

        stats.setPreferredSize(new Dimension(320, 800));
        storyText.setPreferredSize(new Dimension(400, 500));

        input.setPreferredSize(new Dimension(50,50));

        this.getRootPane().setDefaultButton(submit);

        this.add(introPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setVisible(true);

    }

    void setStoryText(String text){
        storyText.setText(text);

    }

    void setContinueText(){
        input.setText("Press ENTER to continue..");
    }

    void setStats(String stats){
        this.stats.setText(stats);
    }

    String getInputText(){
        return input.getText();
    }

    void clearInput(){
        input.setText("");
    }

    void addSubmitListener(ActionListener listenerForSubmit){
        submit.addActionListener(listenerForSubmit);
    }

    void displayErrorMessage(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage);
    }

}

