package com.game.environment;

import com.game.characters.*;
import com.game.characters.Character;
import com.game.core.WorldModel;

public class Battle {

    Character player;
    Enemy enemy;
    Utilities util;
    Character currentTurn;
    World world;
    boolean finished;

    public Battle(Character player, Enemy enemy, World world){
        this.player = player;
        this.enemy = enemy;
        this.world = world;
        finished = false;
        util = new Utilities();
    }

    public Battle(Character player, Enemy enemy, WorldModel world){
        this.player = player;
        this.enemy = enemy;
        finished = false;
        util = new Utilities();
    }

    public void startBattle(){
        util.storyText(String.format("BATTLE MODE INITIATED - PREPARE FOR WAR!\n" +
                       "Let's see who will go first....\n" +
                       "Will it be %s or %s?", player.getName(), enemy.getName())
        );

        util.storyContinue();

        if (util.returnRandomNumber(1, 100) > 50){
            enemy.setTurn(true);
            util.storyText(String.format("It is %s's turn, prepare to get battered!", enemy.getName()));
            currentTurn = enemy;
        } else {
            player.setTurn(true);
            util.storyText(String.format("It is %s's turn! Try not to die.", player.getName()));
            currentTurn = player;
        }
        util.storyContinue();

        if(player.getBattleCount() == 0){
            util.storyText("Before we start let's teach you what things you're able to do in your first battle!");
            System.out.println(String.format("%s - %s\n%s - %s",
                    Utilities.ATTACK_ACTION, "Attack the enemy with your " + player.getEquippedWeapon().getName(),
                    Utilities.CLASS_ABILITY, "Use your class ability " + player.getClassAbility())
            );
            util.storyContinue();
        }
        runTurn();
    }

    private void runTurn() {
        util.storyText(String.format("%s hitpoints: %s\n%s hitpoints: %s",
            player.getName(), player.getHitPoints(), enemy.getName(), enemy.getHitPoints())
        );
        enemy.taunt(player);
        do {
            if (currentTurn.equals(player)) {
                System.out.println("Choose your action");
                util.listInBattleACtions();
                util.performBattleAction(this, world);
                currentTurn.setTurn(false);
                currentTurn = enemy;
            } else {
                util.storyContinue();
                System.out.println(String.format("%s leaps at you with his %s.",
                    enemy.getName(), enemy.getEquippedWeapon().getName())
                );
                util.performEnemyAction(currentTurn, world);
                if (player.getHitPoints() <= 0){
                    System.out.println(
                        String.format("The %s manages to land a hit on you doing %s damage. You have no hitpoints and die a sad death like a loser in the dirt.",
                        enemy.getCharacterType(), enemy.getEquippedWeapon().getDamage())
                    );
                    player.setIsDead(true);
                } else {
                    System.out.println(
                        String.format("The %s manages to land a hit on you doing %s damage. You now have %s hitpoints.",
                        enemy.getCharacterType(), enemy.getEquippedWeapon().getDamage(), player.getHitPoints()));
                    currentTurn = player;
                }
            }

            if (player.isDead()){
                util.storyContinue();
                util.storyText(String.format("You have been defeated by %s. Better lucky next time %s." +
                    "\nGAME OVER!", enemy.getName(), player.getName()
                ));
                finished = true;
            }else if (enemy.isDead()) {
                util.storyContinue();
                util.storyText(String.format("You have defeated %s the %s. Their corpse lays on the floor covered in blood",
                        enemy.getName(), enemy.getCharacterType()));

                if (player.getBattleCount() < 4){
                    player.setLevel(player.getLevel() + 1);
                    util.storyText(String.format("You character has increased to level %s!", player.getLevel()));
                }
                finished = true;
            }
        } while (!finished);
    }
}
