package com.game.environment;

import com.game.characters.Enemy;
import com.game.characters.Character;
import com.game.characters.PlayableCharacter;
import com.game.core.WorldModel;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Utilities {

    static final String ATTACK_ACTION = "attack";
    static final String CLASS_ABILITY = "ability";
    private static final String BATTLE_ACTION = "battle";
    public static final String RUN_ACTION = "run";

    private List<String> inBattleActions = Arrays.asList(ATTACK_ACTION,CLASS_ABILITY);
    private List<String> outOfBattleActions = Arrays.asList(BATTLE_ACTION, RUN_ACTION);


    public void listInBattleACtions(){
        inBattleActions.forEach(a -> System.out.println(a + "\n"));
    }

    public void listOutOfBattleActions(){
        outOfBattleActions.forEach(a -> System.out.println(a + "\n"));
    }

    public String storyText(String text) {
        String border = "--------------------------------------------------------------------------------------------------\n";
        return(String.format("%s%s\n%s", border, text, border));
    }

    private static void clearConsole()
    {
        for(int i = 0; i < 300; i++) {
            System.out.print("\n");
        }
    }

    private void printEnemies(World world) {
        System.out.println("List of enemies:");
        world.getEnemies().stream().filter(enemy -> !enemy.isDead()).forEach(enemy -> System.out.print(enemy.getName() + "\n"));
    }

    private void printEnemies(WorldModel world) {
        System.out.println("List of enemies:");
        world.getEnemies().stream().filter(enemy -> !enemy.isDead()).forEach(enemy -> System.out.print(enemy.getName() + "\n"));
    }

    public void storyContinue() {
        System.out.println(".. Continue ..");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        playerInput();
        clearConsole();
    }

    private String playerInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().toLowerCase().replaceAll("\\s+", "");
    }

    public String performAction(Character player, World world) {
        boolean invalid;
        String action;
        do {
            action = playerInput();
            switch (action) {
                case BATTLE_ACTION:
                    storyText("Who do you choose to attack?");
                    printEnemies(world);
                    boolean invalidChoice2 = true;
                    do {
                        storyText("Remember to type something that actually exists, ya hear?");
                        String choice = playerInput();
                        for (Enemy enemy : world.getEnemies()) {
                            String enemyToCheck = enemy.getName().toLowerCase().replaceAll("\\s+", "");
                            if (enemyToCheck.equals(choice)) {
                                invalidChoice2 = false;
                                Battle battle = new Battle(player, enemy, world);
                                battle.startBattle();
                            }
                        }
                    } while (invalidChoice2);
                    invalid = false;
                    break;
                case RUN_ACTION:
                    storyText("Running away!? Pah, go back to your stinking shack then peasant");
                    invalid = false;
                    break;
                default:
                    storyText("That's not an action you can do here, scum bag!");
                    invalid = true;
            }
        } while (invalid);
        return action;
    }

    public String performAction(Character player, WorldModel world) {
        boolean invalid;
        String action;
        do {
            action = playerInput();
            switch (action) {
                case BATTLE_ACTION:
                    storyText("Who do you choose to attack?");
                    printEnemies(world);
                    boolean invalidChoice2 = true;
                    do {
                        storyText("Remember to type something that actually exists, ya hear?");
                        String choice = playerInput();
                        for (Enemy enemy : world.getEnemies()) {
                            String enemyToCheck = enemy.getName().toLowerCase().replaceAll("\\s+", "");
                            if (enemyToCheck.equals(choice)) {
                                invalidChoice2 = false;
                                Battle battle = new Battle(player, enemy, world);
                                battle.startBattle();
                            }
                        }
                    } while (invalidChoice2);
                    invalid = false;
                    break;
                case RUN_ACTION:
                    storyText("Running away!? Pah, go back to your stinking shack then peasant");
                    invalid = false;
                    break;
                default:
                    storyText("That's not an action you can do here, scum bag!");
                    invalid = true;
            }
        } while (invalid);
        return action;
    }

    public boolean runRoll(){
        if(returnRandomNumber(0, 100) < 80){
            return true;
        }
        return false;
    }

    public void performEnemyAction(Character character, World world) {
        character.attack(world.getPlayableCharacter());
    }

    public void performBattleAction(Battle battle, World world) {
        boolean invalid = true;
        do {
            String input = playerInput();
            switch (input){
                case ATTACK_ACTION:
                System.out.println(String.format("You swing your %s at %s",
                    battle.player.getInventory().getEquippedWeapon().getName(), battle.enemy.getName()));

                int hitpointsBefore = battle.enemy.getHitPoints();
                battle.player.attack(battle.enemy);
                if(battle.enemy.getHitPoints() <=0){
                    storyText(String.format("You attacked %s with your %s for %s damage! You killed %s.",
                            battle.enemy.getName(), battle.player.getInventory().getEquippedWeapon().getName(),
                            battle.player.getInventory().getEquippedWeapon().getDamage(), battle.enemy.getName())
                    );
                    battle.enemy.setIsDead(true);
                }else {
                    storyText(String.format("You attacked %s with your %s for %s damage! They now have %s hitpoints",
                            battle.enemy.getName(), battle.player.getEquippedWeapon().getName(),
                            hitpointsBefore - battle.enemy.getHitPoints(), battle.enemy.getHitPoints()));
                }
                invalid = false;
                break;
                case CLASS_ABILITY:
                    int abilityDamage = battle.player.classAbility(battle.enemy);
                    battle.enemy.setHitPoints(battle.enemy.getHitPoints() - abilityDamage);
                    if(battle.enemy.getHitPoints() <=0){
                        storyText(String.format("You attacked %s with your %s for %s damage! You killed %s.",
                                battle.enemy.getName(), battle.player.getClassAbility(), abilityDamage, battle.enemy.getName())
                        );
                        battle.enemy.setIsDead(true);
                    }else {
                        storyText(String.format("You attacked %s with your %s for %s damage! They now have %s hitpoints",
                                battle.enemy.getName(), battle.player.getClassAbility(), abilityDamage, battle.enemy.getHitPoints()));
                    }
                    invalid = false;
                    break;
            }
        }while (invalid);
    }

    public String toNoSpaceLower(String string){
        return string.toLowerCase().replaceAll("\\s+", "");
    }

    int returnRandomNumber(int min, int max){
        int randomNumber;
        Random random = new Random();
        randomNumber = min + random.nextInt(max);
        return randomNumber;
    }
}