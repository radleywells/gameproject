package com.game.environment;


/*
Minimal checklist:

4. Player attacks

 */

import com.game.characters.Goblin;
import com.game.characters.PlayableCharacter;
import com.game.core.SetupModel;
import com.game.characters.Enemy;
import com.game.characters.Friendly;
import com.game.items.Weapon;

import java.util.ArrayList;

public class World {

    SetupModel setupModel;
    ArrayList<Friendly> friendlies;
    ArrayList<Enemy> enemies;
    Utilities util;
    PlayableCharacter playableCharacter;

    public World(SetupModel setupModel){
        this.setupModel = setupModel;
        friendlies = new ArrayList<>();
        enemies = new ArrayList<>();
        util = new Utilities();
        playableCharacter = setupModel.getPlayers().get(0);
    }

    public SetupModel getSetupModel() {
        return setupModel;
    }

    public void setSetupModel(SetupModel setupModel) {
        this.setupModel = setupModel;
    }

    public ArrayList<Friendly> getFriendlies() {
        return friendlies;
    }

    public void setFriendlies(ArrayList<Friendly> friendlies) {
        this.friendlies = friendlies;
    }

    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(ArrayList<Enemy> enemies) {
        this.enemies = enemies;
    }

    public Utilities getUtil() {
        return util;
    }

    public void setUtil(Utilities util) {
        this.util = util;
    }

    public PlayableCharacter getPlayableCharacter() {
        return playableCharacter;
    }

    public void intro(){

        util.storyText(
            "Welcome to the land of wonders my friends, you must be the ones that have been sent. The Seer\n" +
            "will explain why you have been called here. Watch out there might be some enemies after that fine wooden\n" +
            "equipment you've got there. In fact someone is approaching you now...."
        );

        util.storyContinue();

        util.storyText(
            "You see a person walking up the path towards you up from the river you can see, twisting it's way through the landscape.\n" +
            "The sun is beating down and the woods are flourishing - you ready yourself for the approaching stranger"
        );

        util.storyContinue();

        PlayableCharacter playerOne = setupModel.getPlayers().get(0);
        //npc intro
        Friendly george = new Friendly("George");
        friendlies.add(george);
        setupModel.getPlayers().forEach(george::greet);

        george.talk(String.format(
        "%s, there's some stinking goblins over down there by the bridge, and they're fucking up anyone walking by!\n" +
        "Can you go show 'em a lesson?", playerOne.getName()
        ));

        //print out description of surroundings
        util.storyText(
            "You look down the path you are walking and see the bridge going over the river ahead"
        );

        util.storyContinue();

        george.talk("You're not gonna run are ya champ? You're looking pretty worried....");
        util.storyText("Make your choice warrior");
        util.listOutOfBattleActions();
        String action = util.performAction(playerOne, this);

        if(action.equals(Utilities.RUN_ACTION)){
            util.storyText("You run away from the whole situation like a wimp... some people just aren't mean to be heroes");
            george.talk(String.format("WAIT %s! SHIT THAT GOBLIN IS COMING STRAIGHT FOR YOU!", playerOne.getName()));
        }

        //enemy intro
        Goblin goblin = new Goblin("Gobbo");
        Weapon goblinWeapon = new Weapon(2, 1, "Special Stick", true);
        goblin.getInventory().setEquippedWeapon(goblinWeapon);
        enemies.add(goblin);

    }
}
