package com.game.items;

public class Armor extends Item{

    private int defense;

    public Armor(int defense, String name, boolean equip){

        setDefense(defense);
        setName(name);
        setEquip(equip);
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }
}
