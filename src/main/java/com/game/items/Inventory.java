package com.game.items;

import java.util.ArrayList;

public class Inventory {

    private ArrayList<Item> items;
    private Armor equippedArmor;
    private Weapon equippedWeapon;

    public Inventory() {

        items = new ArrayList<>();
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }

    public Armor getEquippedArmor() {return this.equippedArmor; }

    public void setEquippedArmor(Armor armor){
        this.equippedArmor = armor;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem (Item item){
        items.remove(item);
    }

    public Weapon getEquippedWeapon(){
        return this.equippedWeapon;
    }

    public void setEquippedWeapon(Weapon equippedWeapon) {
        this.equippedWeapon = equippedWeapon;
    }
}
