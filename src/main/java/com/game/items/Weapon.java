package com.game.items;

import com.game.characters.Character;
import com.game.characters.PlayableCharacter;

public class Weapon extends Item{

    private int damage;
    private int range;

    public Weapon(int damage, int range, String name, boolean equip){
        setDamage(damage);
        setRange(range);
        setName(name);
        setEquip(equip);
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public void equipWeapon(Weapon weapon, Character player){

        Inventory inventory =  player.getInventory();
        Weapon weapon2 = inventory.getEquippedWeapon();
        inventory.setEquippedWeapon(weapon2);
    }
}
